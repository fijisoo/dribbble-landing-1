import React, {PureComponent} from "react";
import {LeftSection} from "../layouts/LeftSection";
import {RightSection} from "../layouts/RightSection";

import './Landing.scss';

export class Landing extends PureComponent <{}, { isReadMoreVisible: boolean }> {

    state = {
        isReadMoreVisible: false,
    };

    onReadMoreOpen = () => {
        this.setState({isReadMoreVisible: true}, () => console.log(this.state))
    };

    onReadMoreClose = () => {
        this.setState({isReadMoreVisible: false}, () => console.log(this.state))
    };

    render() {
        return (
            <div className="page-landing">
                <LeftSection onReadMoreOpen={this.onReadMoreOpen}/>
                <RightSection onReadMoreClose={this.onReadMoreClose} isReadMoreVisible={this.state.isReadMoreVisible}/>
            </div>
        )
    }
}