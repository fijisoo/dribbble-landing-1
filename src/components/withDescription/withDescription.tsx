import React, {PureComponent} from "react";
import './withDescription.scss';

interface ImageWithDescriptionProps {
    description: string;
}

export const withDescription = <P extends object>(Component: React.ComponentType<P>) => {

    return class ImageWithDescription extends PureComponent <P & ImageWithDescriptionProps, {}> {
        render() {
            const {description, ...props } = this.props;
            return (
                <div className="description">
                    <div className="description__component">
                        {console.log('hereeee', this.props)}
                        <Component {...props as P}/>
                    </div>
                    <div className="description__text">
                        {description}
                    </div>
                </div>
            )
        }
    }
};