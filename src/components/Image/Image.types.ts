type TRounded = 10 | 20 | 30 | 40 | 50;

export interface ImageProps {
    imgSrc: string;
    alt: string;
    className?: string;
    responsive?: 'width' | 'height';
    rounded?: TRounded;
    fitted?: boolean;
    title?: string;
}

export interface ImageState {
    isLoaded: boolean;
}