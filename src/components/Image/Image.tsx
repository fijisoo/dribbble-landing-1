import React from 'react';
import classnames from 'classnames';

import './Image.scss';
import {ImageProps, ImageState} from "./Image.types";

export class Image extends React.PureComponent<ImageProps, ImageState> {

    constructor(props: ImageProps){
        super(props);

        this.state = {
            isLoaded: false,
        }
    }

    componentDidMount() {
        this.setState({isLoaded: true})
    }

    render() {
        const {className, responsive, rounded, fitted, imgSrc, alt, title} = this.props;
        const imageStyles = classnames('image', className, {
                [`image--responsive-${responsive}`]: !!responsive,
                [`image--rounded-${rounded}`]: !!rounded,
                'image--fitted': fitted,
            },
        );
        return this.state.isLoaded ? <img src={imgSrc} className={imageStyles} alt={alt} title={title}/> : <span>LOADING</span>
    }
}
