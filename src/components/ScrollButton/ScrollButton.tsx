import React from "react";
import classnames from 'classnames';

import './ScrollButton.scss';

export const ScrollButton = ({className, onHover, onClick}: { className: string, onHover?: (e: any) => void, onClick?: (e: any) => void }) => {
    const scrollButtonClasses = classnames('scrollButton', className, {});

    return <span className={scrollButtonClasses} onMouseEnter={(e) => onHover && onHover(e)} onClick={(e) => onClick && onClick(e)}>SCROLL DOWN</span>
};