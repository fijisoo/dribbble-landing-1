export interface TextSectionProps {
    topText: string;
    header: string;
    description: string;
    onReadMoreOpen: () => void;
}