import React from 'react';
import {Button} from "../Button";
import {TextSectionProps} from "./TextSection.types";

import './TextSection.scss';

export const TextSection = ({topText, header, description, onReadMoreOpen}: TextSectionProps) => {
    return (
        <div className="textSection" title={topText}>
            <span className="textSection-top-text">{topText}</span>
            <h1 className="textSection-header">{header}</h1>
            <div className="textSection-description">{description}</div>
            <Button className="textSection-button" theme="blackFilled" fontWeight="bold" onClick={() => onReadMoreOpen()}>Read more</Button>
        </div>
    )
};