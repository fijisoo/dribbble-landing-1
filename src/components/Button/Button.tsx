import React from 'react';
import classnames from 'classnames';
import {ButtonProps} from "./Button.types";

import './Button.scss';

export const Button = ({children, theme = 'black', disabled, type = 'button', fontSize = 'md', fontWeight = 'normal', iconLeft, iconRight, onClick, className}: ButtonProps) => {

    const buttonStyles = classnames('button', className, {
        [`button--${theme}`]: !!theme,
        [`button--${fontSize}`]: !!fontSize,
        [`button--weight-${fontWeight}`]: !!fontWeight,
        [`button--left-arrow`]: !!iconLeft,
        [`button--right-arrow`]: !!iconRight,
    });

    return (
        <button type={type && type} className={buttonStyles} onClick={() => onClick && onClick()} disabled={disabled}>{children}</button>
    );
};