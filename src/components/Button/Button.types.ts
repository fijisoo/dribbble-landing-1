type ButtonTypes = 'button' | 'submit';
type ButtonThemes = 'white' | 'black' | 'whiteFilled' | 'blackFilled';
type FontSizes = 'sm' | 'md' | 'lg';
type FontWeights = 'normal' | 'bold';

export interface ButtonProps {
    children: string;
    type?: ButtonTypes;
    theme?: ButtonThemes;
    fontSize?: FontSizes;
    fontWeight?: FontWeights;
    iconLeft?: boolean;
    iconRight?: boolean;
    disabled?: boolean;
    onClick?: () => void;
    className?: string;
}