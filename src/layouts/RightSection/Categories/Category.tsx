import React, {Component} from "react";
import {ReactComponent as Facebook} from './../../../assets/facebook.svg';
import {ReactComponent as Instagram} from './../../../assets/instagram.svg';
import {ReactComponent as Twitter} from './../../../assets/twitter.svg';
import {Button} from "../../../components/Button";
import {Image} from '../../../components/Image';
import {CategoriesProps, CategoriesState} from "./Categories.types";
import classnames from 'classnames';

import './Categories.scss';

export class Category extends Component <CategoriesProps, CategoriesState> {
    constructor(props: CategoriesProps) {
        super(props);
        console.log('constructor');
        this.state = {
            currentCategory: {
                images: [],
                title: '',
                id: 0
            },
            prevActiveImg: 0,
            currentActiveImg: 0,
            direction: null,
            isAnimation: false,
            isLoaded: false,
        }
    }

    componentDidUpdate(prevProps: CategoriesProps, prevState: CategoriesState){
        console.log('componentDiDUpdate');
        if (prevState.currentCategory.id !== this.props.data.id) {
            this.setState({
                currentActiveImg: 0,
                prevActiveImg: 0,
                currentCategory: this.props.data,
                isLoaded: true,
            })
        }
    }


    componentDidMount(){
        console.log('componentDidMount');
        this.setState({
            isLoaded: true,
            currentCategory: this.props.data,
        })
    }

    handleImageChange = (selectedNumber: number) => {
        const {currentActiveImg} = this.state;
        setTimeout(() => this.setState({direction: null}), 700);
        selectedNumber > currentActiveImg ?
            this.setState({direction: 'up'}, () => this.animateImageChange(selectedNumber)) :
            this.setState({direction: 'down'}, () => this.animateImageChange(selectedNumber));
    };

    animateImageChange = (selectedNumber: number) => {
        this.setState({prevActiveImg: this.state.currentActiveImg}, () => this.setState({currentActiveImg: selectedNumber}));
    };

    render() {
        const categoriesPictureContener = classnames("categories-picture-container", {
            'categories-picture-container--single': this.state.currentCategory.images.length <= 1,
            'categories-picture-container--up': this.state.direction === 'up',
            'categories-picture-container--down': this.state.direction === 'down',
        });

        const {currentActiveImg, prevActiveImg, currentCategory} = this.state;

        return this.state.isLoaded ? (
            <div className="categories">
                <div className="categories-header">
                    {currentCategory.title}
                </div>
                <div className="categories-media">
                    <Facebook fill="#FFFFFF" className="categories-media-icon"/>
                    <Instagram fill="#FFFFFF" className="categories-media-icon"/>
                    <Twitter fill="#FFFFFF" className="categories-media-icon"/>
                </div>
                <div className="categories-img-nav">
                    {currentCategory.images.map((_, key) => {
                        return <Button
                            className={`categories-img-nav-item ${this.state.currentActiveImg === key && 'categories-img-nav-item--active'}`}
                            onClick={() => this.handleImageChange(key)}
                            disabled={this.state.direction !== null || this.state.currentActiveImg === key}>{`0${key + 1}`}</Button>
                    })}
                </div>
                <div className="categories-navigation">
                    <Button theme="white" iconLeft={true} onClick={() => this.props.onPrevCategory()}>PREV</Button>
                    <Button theme="white" iconRight={true} onClick={() => this.props.onNextCategory()}>NEXT</Button>
                </div>
                <div className="categories-picture">
                    <div className="categories-picture-wrapper">
                        <div className={categoriesPictureContener}>
                            {currentCategory.images.length > 1 &&
                            <Image imgSrc={require(`./../../../assets/${currentCategory.images[prevActiveImg]}`)}
                                   alt="image sample"
                                   className={`categories-picture-container-item`}
                                   fitted/>
                            }
                            <Image imgSrc={require(`./../../../assets/${currentCategory.images[currentActiveImg]}`)}
                                   alt="image sample"
                                   className={`categories-picture-container-item`}
                                   fitted/>
                            {currentCategory.images.length > 1 &&
                            <Image imgSrc={require(`./../../../assets/${currentCategory.images[prevActiveImg]}`)}
                                   alt="image sample"
                                   className={`categories-picture-container-item`}
                                   fitted/>
                            }
                        </div>
                    </div>
                </div>
            </div>
        ): (<div>LOADING</div>);
    }
}