export interface CategoryData {
    images: string[];
    id: number;
    title: string;
}

export interface CategoriesProps{
    data: CategoryData;
    onNextCategory: () => void;
    onPrevCategory: () => void;
}

export interface CategoriesState {
    currentCategory: CategoryData;
    prevActiveImg: number;
    currentActiveImg: number;
    direction: 'up' | 'down' | null;
    isAnimation: boolean;
    isLoaded: boolean;
}
