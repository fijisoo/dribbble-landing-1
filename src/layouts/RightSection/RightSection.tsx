import React, {Component} from 'react';
import {ReadMore} from "../ReadMore";
import {Category} from "./Categories";
import img from './../../assets/hairStyle.jpg';
import classnames from 'classnames';
import {RightSectionProps, RightSectionState} from "./RightSection.types";
import {Button} from "../../components/Button";

import './RightSection.scss';

export class RightSection extends Component <RightSectionProps, RightSectionState> {

    state = {
        currentCategoryArrayPosition: 1,
        data: [{
            id: 1,
            title: 'Simple',
            images: ['hairStyle.jpg',
                'hairStyle.jpg',
                'hairStyle.jpg',
                'hairStyle.jpg',
                'hairStyle.jpg']
        }, {
            id: 2,
            title: 'aw',
            images: ['image.png',
                'image.png',
                'image.png']
        }]
    };

    handleNextCategory = () => {
        this.state.currentCategoryArrayPosition >= this.state.data.length ? this.setState({currentCategoryArrayPosition: 1}, () => console.log(this.state.currentCategoryArrayPosition)) : this.setState({currentCategoryArrayPosition: this.state.currentCategoryArrayPosition + 1}, () => console.log(this.state.currentCategoryArrayPosition));
    };

    handlePrevCategory = () => {
        this.state.currentCategoryArrayPosition === 1 ? this.setState({currentCategoryArrayPosition: this.state.data.length}, () => console.log(this.state.currentCategoryArrayPosition)) : this.setState({currentCategoryArrayPosition: this.state.currentCategoryArrayPosition - 1}, () => console.log(this.state.currentCategoryArrayPosition));

    };

    render() {
        const rightSectionOverClasses = classnames('rightSection__over', {
            'rightSection__over--visible': this.props.isReadMoreVisible
        });

        console.error(this.state.data.find(x => x.id === this.state.currentCategoryArrayPosition));
        return (
            <div className="rightSection">
                <div className="rightSection__under">
                    <Category data={this.state.data.find(x => x.id === this.state.currentCategoryArrayPosition) || this.state.data[0]} onPrevCategory={this.handlePrevCategory} onNextCategory={this.handleNextCategory}/>
                </div>
                <div className={rightSectionOverClasses}>
                <Button theme="black" onClick={() => this.props.onReadMoreClose()}
                className="rightSection__over-close-button">X</Button>
                <ReadMore title={'ANYTHING'} subTitle={'ANOTHER ANYTHING'} text={'WHO EVEN CARES ABOUT IT LOL'}>
                We usually makeup to emphasize your feminity. See how to do it right and without unnecessary hand
                movements.
                We usually makeup to emphasize your feminity. See how to do it right and without unnecessary hand
                movements.
                We usually makeup to emphasize your feminity. See how to do it right and without unnecessary hand
                movements.
                We usually makeup to emphasize your feminity. See how to do it right and without unnecessary hand
                movements.
                We usually makeup to emphasize your feminity. See how to do it right and without unnecessary hand
                movements.
                <ReadMore.ImageWithDescription description="description" imgSrc={img} alt="example" fitted/>
                We usually makeup to emphasize your feminity. See how to do it right and without unnecessary hand
                movements.
                We usually makeup to emphasize your feminity. See how to do it right and without unnecessary hand
                movements.
                We usually makeup to emphasize your feminity. See how to do it right and without unnecessary hand
                movements.
                We usually makeup to emphasize your feminity. See how to do it right and without unnecessary hand
                movements.
                We usually makeup to emphasize your feminity. See how to do it right and without unnecessary hand
                movements.
                We usually makeup to emphasize your feminity. See how to do it right and without unnecessary hand
                movements.
                We usually makeup to emphasize your feminity. See how to do it right and without unnecessary hand
                movements.
                We usually makeup to emphasize your feminity. See how to do it right and without unnecessary hand
                movements.
                We usually makeup to emphasize your feminity. See how to do it right and without unnecessary hand
                movements.
                We usually makeup to emphasize your feminity. See how to do it right and without unnecessary hand
                movements.
                We usually makeup to emphasize your feminity. See how to do it right and without unnecessary hand
                movements.
                We usually makeup to emphasize your feminity. See how to do it right and without unnecessary hand
                movements.
                We usually makeup to emphasize your feminity. See how to do it right and without unnecessary hand
                movements.
                We usually makeup to emphasize your feminity. See how to do it right and without unnecessary hand
                movements.
                We usually makeup to emphasize your feminity. See how to do it right and without unnecessary hand
                movements.
                We usually makeup to emphasize your feminity. See how to do it right and without unnecessary hand
                movements.
                We usually makeup to emphasize your feminity. See how to do it right and without unnecessary hand
                movements.
                We usually makeup to emphasize your feminity. See how to do it right and without unnecessary hand
                movements.
                We usually makeup to emphasize your feminity. See how to do it right and without unnecessary hand
                movements.
                We usually makeup to emphasize your feminity. See how to do it right and without unnecessary hand
                movements.
                </ReadMore>
                </div>
            </div>
        )
    }
}