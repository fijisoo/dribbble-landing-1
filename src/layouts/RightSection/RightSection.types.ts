import {CategoryData} from "./Categories/Categories.types";

export interface RightSectionProps {
    isReadMoreVisible: boolean,
    onReadMoreClose: () => void;
}

export interface RightSectionState {
    data: CategoryData[];
    currentCategoryArrayPosition: number;
}