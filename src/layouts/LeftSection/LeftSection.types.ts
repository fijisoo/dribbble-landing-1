export interface LeftSectionState {
    isVisibleBottomScrollButton: boolean;
    currentNode: number;
    itemsNumber: number;
}

export interface LeftSectionProps {
    onReadMoreOpen: () => void;
}
