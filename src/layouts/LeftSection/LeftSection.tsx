import React, {PureComponent} from 'react';
import {TextSection} from "../../components/TextSection";
import {ScrollButton} from "../../components/ScrollButton";
import {LeftSectionProps, LeftSectionState} from "./LeftSection.types";

import './LeftSection.scss';

export class LeftSection extends PureComponent <LeftSectionProps, LeftSectionState> {

    private leftSectionRef: React.RefObject<HTMLDivElement>;

    constructor(props: LeftSectionProps) {
        super(props);
        this.state = {
            isVisibleBottomScrollButton: true,
            currentNode: 0,
            itemsNumber: 5,
        };

        this.leftSectionRef = React.createRef();
    }

    handleScrollAction = (e: any) => {
        if (this.state.currentNode > this.state.itemsNumber - 3) {
            this.setState({isVisibleBottomScrollButton: false});
        } else {
            this.setState({isVisibleBottomScrollButton: true})
        }


        const currentSectionApprox = Math.floor(e.currentTarget.scrollTop / window.innerHeight);

        if ((currentSectionApprox !== this.state.currentNode)) {
            this.setState({currentNode: currentSectionApprox})
        }
    };

    handleScrollInvoke = () => {

        if (this.leftSectionRef.current) {
            const node = this.leftSectionRef.current.children[this.state.currentNode + 1];
            console.log(node);
            node.scrollIntoView();
        }
    };

    render() {
        return (
            <div className="leftSection" ref={this.leftSectionRef} onScroll={(e) => this.handleScrollAction(e)}>
                <TextSection topText={'Beauty'} header={'How to make a perfect hair.'}
                             onReadMoreOpen={this.props.onReadMoreOpen}
                             description={'We usually makeup to emphasize your feminity. See how to do it right and without unnecessary hand movements.'}/>
                <TextSection topText={'Beauty2'} header={'How to make a perfect makeup.'}
                             onReadMoreOpen={this.props.onReadMoreOpen}
                             description={'We usually makeup to emphasize your feminity. See how to do it right and without unnecessary hand movements.'}/>
                <TextSection topText={'Beauty3'} header={'How to make a perfect makeup.'}
                             onReadMoreOpen={this.props.onReadMoreOpen}
                             description={'We usually makeup to emphasize your feminity. See how to do it right and without unnecessary hand movements.'}/>
                <TextSection topText={'Beauty4'} header={'How to make a perfect makeup.'}
                             onReadMoreOpen={this.props.onReadMoreOpen}
                             description={'We usually makeup to emphasize your feminity. See how to do it right and without unnecessary hand movements.'}/>
                <TextSection topText={'Beauty5'} header={'How to make a perfect makeup.'}
                             onReadMoreOpen={this.props.onReadMoreOpen}
                             description={'We usually makeup to emphasize your feminity. See how to do it right and without unnecessary hand movements.'}/>
                {this.state.isVisibleBottomScrollButton &&
                <ScrollButton className="leftSection-scroll-down" onClick={() => this.handleScrollInvoke()}/>}
                <div className="leftSection-footer">
                    Designed by <a href="https://www.facebook.com/Kamil-Gloc-Graphic-Designer-850885198419471/">:  Kamil Gloc - Graphic Designer</a>
                </div>
            </div>
        )
    }
}