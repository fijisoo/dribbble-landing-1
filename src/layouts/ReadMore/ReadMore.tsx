import React, {PureComponent} from "react";
import { Image } from '../../components/Image';
import {withDescription} from "../../components/withDescription/";
import './ReadMore.scss';

const ImageWithDescription = withDescription(Image);

export interface ReadMoreProps {
    title: string;
    subTitle: string;
    text: string;
}

export class ReadMore extends PureComponent <ReadMoreProps, {}> {

    static Image = Image;
    static ImageWithDescription = ImageWithDescription;

    constructor(props: ReadMoreProps) {
        super(props);
    }

    render() {
        const { title, subTitle, children } = this.props;
        return (
            <div className="readMore">
                <div className="readMore__header">
                    <h1 className="readMore__header-text">{title}</h1>
                    <h5 className="readMore__header-description">{subTitle}</h5>
                </div>
                <div className="readMore__content">
                    {children}
                </div>
            </div>
        );
    }
}